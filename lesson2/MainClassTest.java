package lesson2;

import org.junit.Test;

public class MainClassTest
{
    @Test
    public void  testGetClassNumber()
    {
        MainClass mainClass = new MainClass();
        assert mainClass.getClassNumber() > 45 : "Число должно быть меньше 45";
    }
}
