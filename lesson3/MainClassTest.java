package lesson3;

import org.junit.Test;

public class MainClassTest
{
    @Test
    public void  testGetClassString()
    {
        MainClass mainClass = new MainClass();
        String text = mainClass.getClassString();
        int index_1 = text.indexOf("hello");
        int index_2 = text.indexOf("Hello");
        boolean has_hello = index_1 > -1;
        boolean has_Hello = index_2 > -1;
        assert has_hello || has_Hello : "Hello or hello not found";
    }
}
